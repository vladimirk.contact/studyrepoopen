FROM ubuntu:18.04

RUN	apt-get update && apt-get install -y \
	apt-utils \
	wget \
	git \
   && apt-get autoremove -y \
   && rm -rf /var/lib/apt/lists/* 

ARG TOOLS_PATH=/tools
RUN mkdir ${TOOLS_PATH}
WORKDIR ${TOOLS_PATH}




ARG TOOLCHAIN_TARBALL_URL="https://developer.arm.com/-/media/Files/downloads/gnu-rm/10.3-2021.07/gcc-arm-none-eabi-10.3-2021.07-x86_64-linux.tar.bz2"
ARG TOOLCHAIN_PATH=${TOOLS_PATH}/TOOLCHAIN_PATH
RUN wget ${TOOLCHAIN_TARBALL_URL} \
		&& export TOOLCHAIN_TARBALL_FILENAME=$(basename "${TOOLCHAIN_TARBALL_URL}") \
			&& tar -xvf ${TOOLCHAIN_TARBALL_FILENAME} \
			&& mv $(dirname `tar -tf ${TOOLCHAIN_TARBALL_FILENAME} | head -1`) ${TOOLCHAIN_PATH} \
			&& rm -rf ${TOOLCHAIN_PATH}/share/doc \
			&& rm ${TOOLCHAIN_TARBALL_FILENAME}

ENV PATH="${TOOLCHAIN_PATH}/bin:${PATH}"

WORKDIR /build
